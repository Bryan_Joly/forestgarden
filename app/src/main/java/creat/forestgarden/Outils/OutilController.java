package creat.forestgarden.Outils;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import creat.forestgarden.Modele.GestionProjets;
import creat.forestgarden.Modele.Profile;

import static android.content.Context.MODE_PRIVATE;

public class OutilController {

    public static String chargerFichier(Context context, String fileName) {
        FileInputStream fis = null;
        try {
            fis = context.openFileInput(fileName);

            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            return sb.toString();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "erreur : fichier introuvable", Toast.LENGTH_LONG).show();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void sauvegarderFichier(Context context, String data, String fileName) {
        String text = data;
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(fileName, MODE_PRIVATE);
            fos.write(text.getBytes());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Profile getProfileFromFichier(Context context) {
        Gson gson = new Gson();
        String str = OutilController.chargerFichier(context, Profile.FICHIER_UTILISATEUR); //deplacer dans controller
        Profile profile = gson.fromJson(str, Profile.class);
        if (profile == null) {
            profile = new Profile(Profile.CREATEUR_ANONYME);
            sauvegarderProfile(context, profile);
        }
        return profile;
    }

    public static void sauvegarderProfile(Context context, Profile profile) {
        Gson gson = new Gson();
        String str = gson.toJson(profile);
        OutilController.sauvegarderFichier(context, str, Profile.FICHIER_UTILISATEUR);
    }

    public static void sauvegarderProjets(Context context, GestionProjets gestionProjets) {
        Gson gson = new Gson();
        String str = gson.toJson(gestionProjets);
        OutilController.sauvegarderFichier(context, str, GestionProjets.PROJET_FICHIER);
    }

    public static GestionProjets getProjetsFromJson(Context context) {
        Gson gson = new Gson();
        String str = OutilController.chargerFichier(context, GestionProjets.PROJET_FICHIER);
        GestionProjets gestionProjets = gson.fromJson(str, GestionProjets.class);
        if (gestionProjets == null) {
            gestionProjets = new GestionProjets();
        }
        return gestionProjets;
    }
}
