package creat.forestgarden.Outils;

import android.content.Context;

public class OutilVue {

    public static final String TOAST_PLANTE_CREE = "cliquer ou vous souhaitez pour placer votre plante !";
    public static final String TOAST_PROJET_CREER = "Projet Creé ! Cliquez dessus pour lancer";
    public static final String TOAST_PAS_DE_TAG_SELECTIONNE = "Selectionne au minimum 1 tag !";

    public static int dpFromPx(float px, Context context) {
        float flt = px * context.getResources().getDisplayMetrics().density;

        return Math.round((float) flt);
    }
}
