package creat.forestgarden.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import creat.forestgarden.Modele.Plantes.Tags.Tag;
import creat.forestgarden.Activity.Vues.TagSpinner;
import creat.forestgarden.R;

public class TagSpinnerAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;

    private ArrayList<TagSpinner> mTagSpinList;
    private boolean mValeurParDefautDesCheckBox;

    public TagSpinnerAdapter(Context context, ArrayList<Tag> tagList, boolean valeurParDefaut, ArrayList<Tag> listPlanteTag) {
        mLayoutInflater = LayoutInflater.from(context);
        this.mValeurParDefautDesCheckBox = valeurParDefaut;

        creatTagList(tagList, listPlanteTag);

    }

    public TagSpinnerAdapter(Context context, ArrayList<Tag> tagList, boolean valeurParDefaut) {
        mLayoutInflater = LayoutInflater.from(context);
        this.mValeurParDefautDesCheckBox = valeurParDefaut;

        creatTagList(tagList);

    }

    private void creatTagList(ArrayList<Tag> tagList, ArrayList<Tag> listPlanteTag) {
        this.mTagSpinList = new ArrayList<>();
        mTagSpinList.add(null);

        for (Tag tag : tagList) {
            boolean bool = mValeurParDefautDesCheckBox;
            for (Tag tagPlt : listPlanteTag) {
                if (tag.getNom().equals(tagPlt.getNom())) {
                    bool = true;
                    break;
                }
            }
            mTagSpinList.add(new TagSpinner(tag, bool));
        }
    }

    private void creatTagList(ArrayList<Tag> tagList) {
        this.mTagSpinList = new ArrayList<>();
        mTagSpinList.add(null);
        boolean bool = mValeurParDefautDesCheckBox;
        for (Tag tag : tagList) {

            mTagSpinList.add(new TagSpinner(tag, bool));
        }
    }


    @Override
    public int getCount() {
        return mTagSpinList.size();
    }

    @Override
    public Object getItem(int position) {
        return mTagSpinList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            final int index = position;

            convertView = this.mLayoutInflater.inflate(R.layout.spinner_tag_item, parent, false);
            final View view = convertView;
            final CheckBox checkBox = view.findViewById(R.id.itemSpinnerCheckBox);


            TextView text = view.findViewById(R.id.textTagSpinnerItem);
            if (index > 0) {
                mTagSpinList.get(index).setCheckBox(checkBox);
                mTagSpinList.get(index).setBool(this.mTagSpinList.get(index).isCoche());
                text.setText(mTagSpinList.get(index).getTagNom());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (checkBox.isChecked()) {
                            TagSpinner tagSpinner = mTagSpinList.get(index);
                            tagSpinner.setBool(false);
                            checkBox.setChecked(false);
                        } else {
                            TagSpinner tagSpinner = mTagSpinList.get(index);
                            tagSpinner.setBool(true);
                            checkBox.setChecked(true);


                        }
                    }
                });
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkBox.isChecked()) {
                            checkBox.setChecked(false);
                        } else {

                            checkBox.setChecked(true);
                        }
                    }
                });
            } else {
                text.setText("Selects TAGS");
                checkBox.setVisibility(View.INVISIBLE);
            }


        }


        return convertView;
    }

    public ArrayList<Tag> getTagsCoche() {
        ArrayList<Tag> listTag = new ArrayList<>();

        for (TagSpinner tagSpin : this.mTagSpinList) {
            if (tagSpin != null) {
                if (tagSpin.isCoche()) {
                    listTag.add(tagSpin.getTag());
                }
            }
        }

        return listTag;

    }

    public void selectionnerTag(ArrayList<Tag> tags) {
        for (int i = 0; i < tags.size(); i++) {
            for (int j = 0; j < mTagSpinList.size(); i++) {
                if (tags.get(i).getNom().equals(mTagSpinList.get(j).getTagNom())) {
                    mTagSpinList.get(j).setBool(true);
                }
            }
        }
    }
}
