package creat.forestgarden.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import creat.forestgarden.Modele.Plantes.Tags.Tag;
import creat.forestgarden.Activity.Vues.TagSpinner;
import creat.forestgarden.R;

public class TagAdapterDeprecated extends BaseAdapter {

    private LayoutInflater mLayoutInflater;

    private ArrayList<TagSpinner> mTagSpinList;
    private boolean valeurParDefautDesCheckBox;

    public TagAdapterDeprecated(Context context, ArrayList<Tag> tagList, boolean valeurParDefaut) {
        mLayoutInflater = LayoutInflater.from(context);
        this.valeurParDefautDesCheckBox = valeurParDefaut;
        creatTagList(tagList);
    }

    private void creatTagList(ArrayList<Tag> tagList) {
        this.mTagSpinList = new ArrayList<>();
        mTagSpinList.add(null);
        for (Tag tag : tagList) {
            TagSpinner tagSpin = new TagSpinner(tag, this.valeurParDefautDesCheckBox);
            mTagSpinList.add(tagSpin);
            tagSpin.setBool(true);
        }
    }

    @Override
    public int getCount() {
        return mTagSpinList.size();
    }

    @Override
    public TagSpinner getItem(int position) {
        return mTagSpinList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final int index = position;
            convertView = this.mLayoutInflater.inflate(R.layout.spinner_tag_item, parent, false);


            final CheckBox checkBox = convertView.findViewById(R.id.itemSpinnerCheckBox);
            TextView text = convertView.findViewById(R.id.textTagSpinnerItem);

            if (index > 0) {
                text.setText(mTagSpinList.get(index).getTagNom());
                checkBox.setChecked(valeurParDefautDesCheckBox);
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkBox.isChecked()) {
                            TagSpinner tagSpinner = mTagSpinList.get(index);
                            tagSpinner.setBool(true);
                        } else {
                            TagSpinner tagSpinner = mTagSpinList.get(index);
                            tagSpinner.setBool(false);
                        }
                    }
                });
            } else {
                text.setText("TAGS");
                checkBox.setVisibility(View.INVISIBLE); // la1er ligne pas besoin de check box juste du text pour l utilisateur
            }
            //checkBox.setTag(tagList.get(position));


        }


        return convertView;
    }


    public ArrayList<Tag> getTagsCoche() {
        ArrayList<Tag> listTag = new ArrayList<>();

        for (TagSpinner tagSpin : this.mTagSpinList) {
            if (tagSpin != null) {
                if (tagSpin.isCoche()) {
                    listTag.add(tagSpin.getTag());
                }
            }
        }

        return listTag;

    }


}

