package creat.forestgarden.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Random;

import creat.forestgarden.Activity.ProjetActivity;
import creat.forestgarden.Modele.GestionPlateau.Cellule;
import creat.forestgarden.Modele.GestionPlateau.Plateau;
import creat.forestgarden.R;

public class MapAdapter extends BaseAdapter {

    private Plateau mPlateau;
    private LayoutInflater mMayoutInflater;
    private ProjetActivity mMenuActivity;

    public MapAdapter(Plateau plateau, Context context, ProjetActivity menuActivity) {
        this.mPlateau = plateau;
        mMayoutInflater = LayoutInflater.from(context);
        mMenuActivity = menuActivity;
    }

    @Override
    public int getCount() {
        return mPlateau.getGrille().length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.mMayoutInflater.inflate(R.layout.tile_plateau, parent, false);

            ImageButton img = convertView.findViewById(R.id.imageSol);

            img.setImageResource(R.drawable.grass);
            rotate(img);


            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Cellule cel = mPlateau.getGrille()[position];
                    v.setTag(cel);
                    cliqueCellule(v);
                }
            });
        }


        return convertView;
    }

    private void cliqueCellule(View view) {
        mMenuActivity.cliqueOnCell(view);
    }

    private void rotate(ImageView imageView) {
        Random rnd = new Random();
        int switchValue = rnd.nextInt(4);
        switch (switchValue) {
            case 0:
                imageView.setRotation(0);
                break;
            case 1:
                imageView.setRotation(90);
                break;
            case 2:
                imageView.setRotation(180);
                break;
            case 3:
                imageView.setRotation(270);
                break;
        }

    }
}
