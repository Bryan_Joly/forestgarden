package creat.forestgarden.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import creat.forestgarden.R;

public class ImageAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private ArrayList<Integer> listImage;

    public ImageAdapter(Context context, ArrayList<Integer> list) {
        mLayoutInflater = LayoutInflater.from(context);
        listImage = list;
    }


    @Override
    public int getCount() {
        return listImage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            convertView = this.mLayoutInflater.inflate(R.layout.spinner_image, parent, false);

            ImageView image = convertView.findViewById(R.id.spinnerimageplante);
            image.setImageResource(listImage.get(position));

        }


        return convertView;
    }
}
