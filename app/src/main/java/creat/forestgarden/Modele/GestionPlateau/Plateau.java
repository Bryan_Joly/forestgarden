package creat.forestgarden.Modele.GestionPlateau;

import java.io.Serializable;
import java.util.ArrayList;

import creat.forestgarden.Modele.Plantes.Plante;

public class Plateau implements Serializable {

    private Cellule[] mGrille;

    private int mX;
    private int mY;

    private int mTailleImage = 50;
    private ArrayList<Plante> mListPlante;

    public Plateau(int x, int y) {
        mGrille = new Cellule[x * y];
        this.mX = x;
        this.mY = y;
        mListPlante = new ArrayList<>();
        remplirTableau();
    }

    public void remplirTableau() {
        for (int i = 0; i < mGrille.length; i++) {
            mGrille[i] = new Cellule(i);
        }
    }

    public Cellule[] getGrille() {
        return mGrille;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public int getTailleImage() {
        return this.mTailleImage;
    }

    public void ajouterPlante(Plante plante) {
        this.mListPlante.add(plante);
    }

    public void modifierPlante(Plante plante) {
        for (Plante plt : mListPlante) {
            if (plt.getId().equals(plante.getId())) {
                plt = plante;
                break;
            }
        }
    }
}
