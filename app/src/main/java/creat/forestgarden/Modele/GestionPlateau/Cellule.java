package creat.forestgarden.Modele.GestionPlateau;

import java.io.Serializable;

import creat.forestgarden.Modele.Plantes.Arbre;
import creat.forestgarden.Modele.Plantes.Plante;

public class Cellule implements Serializable {

    private Plante mPlante;
    private int mIndex; //sa position dans le plateau

    public Cellule(int index) {
        this.mIndex = index;
    }

    public void setPlante(Plante plante) {
        this.mPlante = plante;
    }

    public int getIndex() {
        return mIndex;
    }

    public Plante getPlante() {
        return this.mPlante;
    }
}
