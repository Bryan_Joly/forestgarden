package creat.forestgarden.Modele;


import java.io.Serializable;
import java.util.UUID;

import creat.forestgarden.Modele.GestionPlateau.Plateau;
import creat.forestgarden.Modele.Plantes.Tags.GestionTags;

public class Projet implements Serializable {


    private ProjetCaracteristique mProjetCarac;
    private Plateau mPlateau;
    private UUID mID;
    private GestionTags mGestionTags;


    public Projet(String nomProjet, int tailleX, int tailleY, String region, long valeurCreateur, String description) {
        int aire = tailleX * tailleY;
        String taille = aire + "mètres carrés";
        String nomCreateur;
        if (valeurCreateur == 0) {
            nomCreateur = "BEREZINA";
        } else {
            nomCreateur = Profile.CREATEUR_ANONYME;
        }
        this.mGestionTags = new GestionTags(true);
        this.mProjetCarac = new ProjetCaracteristique(nomProjet, nomCreateur, region, taille, description);
        this.mPlateau = new Plateau(tailleX, tailleY);

        mID = UUID.randomUUID();
    }

    public ProjetCaracteristique getProjetCarac() {
        return mProjetCarac;
    }

    public Plateau getPlateau() {
        return mPlateau;
    }

    public UUID getId() {
        return mID;
    }

    public GestionTags getGestionTag() {
        return mGestionTags;
    }

    public void setGestionTag(GestionTags gestionTags) {
        this.mGestionTags = gestionTags;
    }
}
