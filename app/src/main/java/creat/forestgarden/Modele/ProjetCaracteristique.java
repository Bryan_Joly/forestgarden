package creat.forestgarden.Modele;

import java.io.Serializable;
import java.util.UUID;

public class ProjetCaracteristique implements Serializable {


    private String mNomProjet;
    private String mNomCreateur;
    private UUID mIDProjet; // A generer par le server
    private String mRegion;
    private String mTaille;
    private String mDescription;

    public ProjetCaracteristique(String nomProjet, String nomCreateur, String region, String taille, String description) {
        this.mNomProjet = nomProjet;
        this.mNomCreateur = nomCreateur;
        this.mTaille = taille;
        this.mDescription = description;
        setRegion(region);
    }

    public String getNomProjet() {
        return mNomProjet;
    }

    public String getNomCreateur() {
        return mNomCreateur;
    }

    public UUID getIDProjet() {
        return mIDProjet;
    }

    public String getRegion() {
        return mRegion;
    }

    public String getTaille() {
        return mTaille;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setRegion(String region) {
        if (!region.isEmpty()) {
            this.mRegion = region;
        } else {
            this.mRegion = "Non communiqué";
        }
    }
}
