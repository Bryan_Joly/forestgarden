package creat.forestgarden.Modele.Plantes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import creat.forestgarden.Modele.Plantes.Tags.Tag;

public class Plante implements Serializable {


    protected String mNom;
    protected Espece mEspece;
    protected int mHauteur;
    protected int mDiametre;
    protected ArrayList<Tag> mListTag;
    protected int idAffichage;
    protected UUID mID;

    public Plante(String nom, Espece espece, int hauteur, int diametre, ArrayList<Tag> tags, int idImage) {
        this.mNom = nom;
        this.mEspece = espece;
        this.mHauteur = hauteur;
        this.mDiametre = diametre;
        this.mListTag = tags;
        this.idAffichage = idImage;
        mID = UUID.randomUUID();
    }


    public ArrayList<Tag> getTags() {
        return this.mListTag;
    }

    public int getHauteur() {
        return mHauteur;
    }

    public UUID getId() {
        return this.mID;
    }

    public int getDiametre() {
        return mDiametre;
    }

    public int getIdAffichage() {
        return idAffichage;
    }

    public String getNom() {
        return mNom;
    }

    public Espece getEspece() {
        return mEspece;
    }

    public void setNom(String mNom) {
        this.mNom = mNom;
    }

    public void setEspece(Espece mEspece) {
        this.mEspece = mEspece;
    }

    public void setHauteur(int mHauteur) {
        this.mHauteur = mHauteur;
    }

    public void setDiametre(int mDiametre) {
        this.mDiametre = mDiametre;
    }

    public void setListTag(ArrayList<Tag> mListTag) {
        this.mListTag = mListTag;
    }

    public void setIdAffichage(int idAffichage) {
        this.idAffichage = idAffichage;
    }
}
