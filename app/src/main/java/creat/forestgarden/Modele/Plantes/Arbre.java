package creat.forestgarden.Modele.Plantes;

import java.io.Serializable;
import java.util.ArrayList;

import creat.forestgarden.Modele.Plantes.Tags.Tag;

public abstract class Arbre extends Plante implements Serializable {


    public Arbre(String nom, Espece espece, int hauteur, int diametre, ArrayList<Tag> tags, int id) {
        super(nom, espece, hauteur, diametre, tags, id);
    }
}
