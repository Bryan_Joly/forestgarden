package creat.forestgarden.Modele.Plantes.Tags;

import java.io.Serializable;

public class Tag implements Serializable {
    private String mNom;

    public Tag(String value) {
        this.mNom = value;
    }

    public String getNom() {
        return mNom;
    }

}
