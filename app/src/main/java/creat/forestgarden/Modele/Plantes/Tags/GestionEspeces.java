package creat.forestgarden.Modele.Plantes.Tags;

import java.util.ArrayList;

import creat.forestgarden.Modele.Plantes.Espece;

public class GestionEspeces {

    public static ArrayList<Espece> listEspece = new ArrayList<>();


    public static void ajouterEspeceDEVELOP() //utilisation lors du devellopement
    {
        listEspece.add(new Espece("Prunier", "Prunus domestica"));
        listEspece.add(new Espece("Cerisier", "Prunus"));
        listEspece.add(new Espece("Pommier", "Malus"));
        listEspece.add(new Espece("Poirier", "Pyrus communis"));
        listEspece.add(new Espece("Framboisier", "Rubus idaeus"));
        listEspece.add(new Espece("Groseiller", "Ribes"));
        listEspece.add(new Espece("Myrtiller", "Vaccinium myrtillus"));
        listEspece.add(new Espece("Vigne", "Vitis vinifera"));
    }

    public static Espece getEspece(int index) {
        return listEspece.get(index);
    }
}
