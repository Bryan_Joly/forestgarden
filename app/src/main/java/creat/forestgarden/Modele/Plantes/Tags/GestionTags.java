package creat.forestgarden.Modele.Plantes.Tags;

import java.io.Serializable;
import java.util.ArrayList;

public class GestionTags implements Serializable {

    private ArrayList<Tag> mListTag = new ArrayList<>();

    public GestionTags(boolean tagDeBase) {
        if (tagDeBase == true) {
            ArrayList<Tag> listBase = getTagDeBase();
            for (Tag tag : listBase) {
                mListTag.add(tag);
            }
        }
    }

    private ArrayList<Tag> getTagDeBase() //utilisation lors du devellopement
    {
        ArrayList<Tag> listTag = new ArrayList<>();
        listTag.add(new Tag("Fruitier"));
        listTag.add(new Tag("Accumulateur de mineraux"));
        listTag.add(new Tag("Arbre sacrificiel"));
        listTag.add(new Tag("Fixateaur d'azote"));
        listTag.add(new Tag("Porteur de liane"));
        listTag.add(new Tag("Trogne"));
        listTag.add(new Tag("Arbre légume"));

        return listTag;
    }

    public void ajouterTag(Tag tag) {
        this.mListTag.add(tag);
    }

    public ArrayList<Tag> getTagList() {
        return mListTag;
    }
}
