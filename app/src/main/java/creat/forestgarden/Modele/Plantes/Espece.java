package creat.forestgarden.Modele.Plantes;

import java.io.Serializable;

public class Espece implements Serializable {

    private String mNom;
    private String mNomLatin;

    public Espece(String nom) {
        this.mNom = nom;
    }

    public Espece(String nom, String nomLatin) {
        this.mNom = nom;
        this.mNomLatin = nomLatin;
    }

    public String getNom() {
        return mNom;
    }

    public String getNomLatin() {
        return mNomLatin;
    }

    public void setNomLatin(String mNomLatin) {
        this.mNomLatin = mNomLatin;
    }
}
