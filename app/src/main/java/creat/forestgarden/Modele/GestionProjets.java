package creat.forestgarden.Modele;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;

import creat.forestgarden.Modele.Projet;

public class GestionProjets {

    public static final String PROJET_FICHIER = "PROJET_FICHIER";

    private ArrayList<Projet> listProjet;

    public GestionProjets() {
        listProjet = new ArrayList<>();
        loadProjects();
    }

    private void loadProjects() {
        listProjet.add(new Projet("forestGarden", 5, 10, "", 0, "bla bla"));


    }

    public ArrayList<Projet> getListProjet() {
        return listProjet;
    }

    public int getNbrProjet() {

        return listProjet.size();
    }

    public void ajouterProjet(Projet projet) {
        listProjet.add(projet);

    }

    public void mettreAJourProjet(Projet projet) {
        for (int i = 0; i < listProjet.size(); i++) {
            if (projet.getId().equals(listProjet.get(i).getId())) {
                listProjet.set(i, projet);
            }
        }
    }

}
