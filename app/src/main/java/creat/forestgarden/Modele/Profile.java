package creat.forestgarden.Modele;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;

import com.google.gson.Gson;

import java.io.Serializable;


import creat.forestgarden.R;

public class Profile implements Serializable {

    public static final String CREATEUR_ANONYME = "Anonyme";
    public static final String FICHIER_UTILISATEUR = "FICHIER_UTILISATEUR";
    public static final String NON_COMMUNIQUER = "NON_COMMUNIQUER";

    private String mPseudo = "";
    private String mRegion = "";
    private String mEmail = "";

    /*GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();*/

    public Profile(String pseudo) {
        this.mPseudo = pseudo;
        mEmail = "NON_COMMUNIQUER";
        mRegion = "NON_COMMUNIQUER";


    }


    public String getPseudo() {
        return mPseudo;
    }

    public String getRegion() {
        return mRegion;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setPseudo(String mPseudo) {
        this.mPseudo = mPseudo;
    }

    public void setRegion(String mRegion) {
        this.mRegion = mRegion;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }


}
