package creat.forestgarden.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import creat.forestgarden.Adapter.ImageAdapter;
import creat.forestgarden.Adapter.TagSpinnerAdapter;

import creat.forestgarden.Modele.Plantes.Espece;
import creat.forestgarden.Modele.Plantes.Plante;
import creat.forestgarden.Modele.Plantes.Tags.GestionEspeces;
import creat.forestgarden.Modele.Plantes.Tags.GestionTags;
import creat.forestgarden.Modele.Plantes.Tags.Tag;
import creat.forestgarden.Outils.OutilVue;
import creat.forestgarden.R;

public class PlanteActivity extends AppCompatActivity {

    public static final int REQUEST_PLANTE = 1;
    public static final String ACTION_AJOUTER_PLANTE = "add";
    public static final String ACTION_MODIF_PLANTE = "modif";
    public static final int REQUEST_STOP = 0;
    public static final int REQUEST_PLANTE_CREE = 2;
    public static final int REQUEST_PLANTR_CREE_PLUS_TAG = 3;
    public static final int REQUEST_PLANTE_MODIF = 4;
    public static final String EXT_PLANTE = "plante";
    public static final String EXT_PLANTE_MODIF = "plantemodif";
    public static final String EXT_GESTION_TAG = "EXT_GESTION_TAG";
    public static final String EXT_GESTION_TAG_MODIF = "EXT_GESTION_TAG_MODIF";


    private ArrayList<Integer> listImage;

    private EditText mInputNomPlante;
    private EditText mInputHauteur;
    private EditText mInputDiametre;

    private Spinner mSpinnerTags; // permet de choisir les tag de la plante

    private Spinner mSpinneImagePlante;
    private Spinner mSpinnerEspeces;

    private TagSpinnerAdapter mTagSpinnerAdapter;
    private LinearLayout mLayoutTag;
    private TextView mTitreActivite; // titre pour savoir si on ajoute ou modifie

    private ImageButton mBtnValiderActivite; // on quitte avec un modif ou creation
    private ImageButton mBtnStopActivite; // on quitte sans rien faire
    private Plante mModifPlante;
    private GestionTags mGestionTag;
    private boolean mTagAjoute = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        mGestionTag = (GestionTags) bundle.get(PlanteActivity.EXT_GESTION_TAG);

        setContentView(R.layout.activity_plante);
        setListImage();
        setUI();

        if (this.getIntent().getAction() == ACTION_AJOUTER_PLANTE) {
            setSpinnerTagValeur();

        } else if (this.getIntent().getAction() == ACTION_MODIF_PLANTE) {
            mModifPlante = (Plante) bundle.get(PlanteActivity.EXT_PLANTE_MODIF);
            setPlanteValueToUI(mModifPlante);
            setSpinnerTagValeur(mModifPlante);
            setSpinnerEspeceValeurModif();
        }

    }

    private void setSpinnerEspeceValeurModif() {

        int i;
        for (i = 0; i < GestionEspeces.listEspece.size(); i++) {
            String nomPlante = this.mModifPlante.getEspece().getNom();
            String nomInList = GestionEspeces.listEspece.get(i).getNom();
            if (nomInList.toString().equals(nomPlante.toString())) {
                int a = 0;

                break;
            }
        }

        this.mSpinnerEspeces.setSelection(i);
    }

    private void setSpinnerTagValeur(Plante plante) {
        this.mTagSpinnerAdapter = new TagSpinnerAdapter(this, mGestionTag.getTagList(), false, plante.getTags());
        mSpinnerTags.setAdapter(mTagSpinnerAdapter);
    }

    private void setPlanteValueToUI(Plante plante) {

        this.mInputNomPlante.setText(plante.getNom());
        this.mInputHauteur.setText(String.valueOf(plante.getHauteur()));
        this.mInputDiametre.setText(String.valueOf(plante.getDiametre()));

    }

    private void setUI() {

        setLayout();
        setSpinner();
        setEditText();
        setButtons();
    }

    private void setLayout() {
        mLayoutTag = findViewById(R.id.layoutNouveauTag);
        mLayoutTag.getBackground().setAlpha(200);
        mLayoutTag.setVisibility(View.INVISIBLE);

        LinearLayout layout = findViewById(R.id.layoutTagTextZone);
        layout.getBackground().setAlpha(255);
    }

    private void setListImage() {
        listImage = new ArrayList<>();
        listImage.add(R.drawable.plante_arbre_1);
        listImage.add(R.drawable.plante_arbre_2);
        listImage.add(R.drawable.plante_arbre_3);
        listImage.add(R.drawable.plante_arbre_4);
        listImage.add(R.drawable.plante_arbre_mort_1);
        listImage.add(R.drawable.plante_arbre_2);
        listImage.add(R.drawable.plante_sapin_1);
        listImage.add(R.drawable.plante_sapin_2);
        listImage.add(R.drawable.plante_sapin_3);

    }

    private void setButtons() {
        this.mBtnStopActivite = findViewById(R.id.quitterAjouterPlante);
        this.mBtnValiderActivite = findViewById(R.id.validerAjouterPlante);
        ImageButton imageButtonTags = findViewById(R.id.boutonAjouterTag);


        setButtonStopActiviteListener();
        setButtonValiderActivite();
        imageButtonTags.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cliqueNouveauTag();
            }
        });

    }

    private void setButtonStopActiviteListener() {
        this.mBtnStopActivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBtnStopActiviteAction();
            }
        });

    }

    private void setButtonValiderActivite() {
        this.mBtnValiderActivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBtnValiderActiviteAction();
            }
        });
    }

    private void cliqueNouveauTag() {
        mLayoutTag.setVisibility(View.VISIBLE);

        this.mBtnStopActivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quitterLayoutTag();
            }
        });

        this.mBtnValiderActivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajouterTag();
            }
        });
    }

    private void ajouterTag() {
        EditText editText = findViewById(R.id.nouveauTag);
        mGestionTag.ajouterTag(new Tag(editText.getText().toString()));
        Toast.makeText(this, "Nouveau tag ajouter : " + editText.getText(), Toast.LENGTH_SHORT).show();
        this.mTagAjoute = true;
        setSpinnerTagValeur();
        quitterLayoutTag();
    }

    private void quitterLayoutTag() {
        fermerPanelTag();
    }

    private void fermerPanelTag() {
        this.mLayoutTag.setVisibility(View.INVISIBLE);
        setButtonStopActiviteListener();
        setButtonValiderActivite();
    }

    private void setBtnValiderActiviteAction() {
        Intent intent = new Intent();
        if (isTagSlectionne()) {
            Plante plante;

            if (this.getIntent().getAction() == ACTION_MODIF_PLANTE) {
                setUIModification();
                plante = this.mModifPlante;
                intent.putExtra(EXT_PLANTE, plante);
                setResult(REQUEST_PLANTE_MODIF, intent);
            } else if (this.getIntent().getAction() == ACTION_AJOUTER_PLANTE) {
                plante = creerPlanteDepuisVue();
                if (mTagAjoute == true) {
                    intent.putExtra(EXT_GESTION_TAG_MODIF, mGestionTag);
                    intent.putExtra(EXT_PLANTE, plante);
                    setResult(REQUEST_PLANTR_CREE_PLUS_TAG, intent);
                } else {
                    intent.putExtra(EXT_PLANTE, plante);
                    setResult(REQUEST_PLANTE_CREE, intent);
                }
            }

            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, OutilVue.TOAST_PAS_DE_TAG_SELECTIONNE, Toast.LENGTH_LONG).show();

        }

    }

    private boolean isTagSlectionne() {
        if (this.mTagSpinnerAdapter.getTagsCoche().size() > 0) {
            return true;
        }
        return false;
    }

    private Plante creerPlanteDepuisVue() {
        Plante plante;
        plante = new Plante(
                this.getNomDepuisInput(),
                this.getEspeceDepuisSpinner(),
                Integer.parseInt(this.mInputHauteur.getText().toString()),
                Integer.parseInt(this.mInputDiametre.getText().toString()),
                this.mTagSpinnerAdapter.getTagsCoche(),
                this.getImageId()
        );
        return plante;
    }

    private void setUIModification() {
        mModifPlante.setNom(this.getNomDepuisInput());
        mModifPlante.setDiametre(Integer.parseInt(this.mInputHauteur.getText().toString()));
        mModifPlante.setHauteur(Integer.parseInt(this.mInputDiametre.getText().toString()));
        mModifPlante.setListTag(this.mTagSpinnerAdapter.getTagsCoche());
        mModifPlante.setIdAffichage(this.getImageId());
    }

    private int getImageId() {
        int imageID = listImage.get(mSpinneImagePlante.getSelectedItemPosition());
        switch (imageID) {
            case R.drawable.plante_arbre_1:
                return 1;

            case R.drawable.plante_arbre_2:
                return 2;

            case R.drawable.plante_arbre_3:
                return 3;

            case R.drawable.plante_arbre_4:
                return 4;

            case R.drawable.plante_arbre_mort_1:
                return 5;

            case R.drawable.plante_arbre_mort_2:
                return 6;

            case R.drawable.plante_sapin_1:
                return 7;

            case R.drawable.plante_sapin_2:
                return 8;

            case R.drawable.plante_sapin_3:
                return 9;

            default:
                return 0;
        }
    }

    private Espece getEspeceDepuisSpinner() {
        Espece espece;
        long index = mSpinnerEspeces.getSelectedItemId();

        espece = GestionEspeces.getEspece((int) index);

        return espece;
    }

    private String getNomDepuisInput() {
        return this.mInputNomPlante.getText().toString();
    }

    private void setBtnStopActiviteAction() {
        setResult(REQUEST_STOP);

        super.onBackPressed();

    }

    private void setSpinner() {
        this.mSpinnerEspeces = findViewById(R.id.spinnerEspece);
        this.mSpinnerTags = findViewById(R.id.spinnerTag);
        this.mSpinneImagePlante = findViewById(R.id.spinnerImagePlante);

        setSpinnerEspecesValeur();
        setSpinnerImages();
    }

    private void setSpinnerImages() {

        ImageAdapter arrayAdapter = new ImageAdapter(this, this.listImage);
        mSpinneImagePlante.setAdapter(arrayAdapter);
    }

    private void setSpinnerTagValeur() {
        this.mTagSpinnerAdapter = new TagSpinnerAdapter(this, mGestionTag.getTagList(), false);
        mSpinnerTags.setAdapter(mTagSpinnerAdapter);
    }


    private void setSpinnerEspecesValeur() {
        ArrayList<String> list = new ArrayList<>();

        for (Espece espece : GestionEspeces.listEspece) {
            list.add(espece.getNom());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        mSpinnerEspeces.setAdapter(arrayAdapter);
    }

    private void setEditText() {
        this.mInputNomPlante = findViewById(R.id.inputNomPlante);
        this.mInputDiametre = findViewById(R.id.inputDiametrePlante);
        this.mInputHauteur = findViewById(R.id.inputHauteurPlante);
    }
}