package creat.forestgarden.Activity.Controllers;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import creat.forestgarden.Modele.Plantes.Plante;

public class PlanteImage {
    private Plante mPlante;
    private ImageButton mImageButton;
    private int mIndex;

    public PlanteImage(Plante plante, ImageButton imageView, int index) {
        mPlante = plante;
        mImageButton = imageView;
        this.mIndex = index;
    }

    public Plante getPlante() {
        return mPlante;
    }

    public void setVisibilite(Boolean bool) {
        if (bool == true) {
            mImageButton.setVisibility(View.VISIBLE);
        } else {
            mImageButton.setVisibility(View.INVISIBLE);
        }
    }

    public int getIndex() {
        return mIndex;
    }

    public ImageButton getImage() {
        return this.mImageButton;
    }
}
