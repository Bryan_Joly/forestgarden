package creat.forestgarden.Activity.Controllers;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import creat.forestgarden.Activity.DTO.DTOPlacementPlante;
import creat.forestgarden.Modele.GestionPlateau.Cellule;
import creat.forestgarden.Modele.GestionPlateau.Plateau;
import creat.forestgarden.Modele.Plantes.Plante;
import creat.forestgarden.Modele.Plantes.Tags.GestionTags;
import creat.forestgarden.Modele.Projet;
import creat.forestgarden.Outils.OutilVue;
import creat.forestgarden.R;

public class ControllerPlante {


    private DTOPlacementPlante mPlacementArbre;

    private RelativeLayout mPlanteLayout;
    private Context mContext;

    private Plateau mPlateau;
    private ControllerFilter mControllerFilter;
    private ImageButton mBouttonAnnulerPlante;

    private ControllerPlanteListener mListener;

    public ControllerPlante(RelativeLayout planteLayout, Context context, Projet projet, View vueFiltre, ImageButton annulerPlante, ControllerPlanteListener listener) {
        this.mBouttonAnnulerPlante = annulerPlante;
        setButtonAction();
        this.mListener = listener;
        this.mPlanteLayout = planteLayout;
        this.mContext = context;
        mPlacementArbre = new DTOPlacementPlante();
        this.mPlateau = projet.getPlateau();
        this.mControllerFilter = new ControllerFilter(vueFiltre, projet.getGestionTag(), this);
        placeArbreDejaDansModele();
    }

    private void placeArbreDejaDansModele() {
        for (int i = 0; i < this.mPlateau.getGrille().length; i++) {
            if (this.mPlateau.getGrille()[i].getPlante() != null) {
                Plante plante = this.mPlateau.getGrille()[i].getPlante();
                ImageButton image = new ImageButton(mContext);

                placerImageArbre(i, plante, image);

            }
        }
    }

    public void setButtonAction() {
        this.mBouttonAnnulerPlante.setVisibility(View.INVISIBLE);

        this.mBouttonAnnulerPlante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlacementArbre(false, null);
            }
        });
    }

    public void cliqueCellule(View view) {
        if (mPlacementArbre.isPlacerArbre() == true) {
            Cellule cel = (Cellule) view.getTag();
            Plante plante = mPlacementArbre.getPlante();
            this.mPlateau.ajouterPlante(plante);
            cel.setPlante(plante);

            ImageButton image = new ImageButton(mContext);
            placerImageArbre(cel.getIndex(), plante, image);


            setPlacementArbre(false);
        }
    }

    public void setPlacementArbre(boolean bool) {
        this.mPlacementArbre.setPlacerArbre(bool);

        if (bool == true) {
            this.mBouttonAnnulerPlante.setVisibility(View.VISIBLE);
        } else {
            this.mBouttonAnnulerPlante.setVisibility(View.INVISIBLE);
        }
    }

    public void setPlacementArbre(boolean bool, Plante plante) {
        this.mPlacementArbre.setPlante(plante);
        this.setPlacementArbre(bool);
    }

    public void placerImageArbre(int index, final Plante plante, ImageButton image) {

        PlanteImage planteImage = new PlanteImage(plante, image, index);
        mControllerFilter.addPlanteImage(planteImage);
        positionnerImage(planteImage);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.ICliquePlante(plante);
            }
        });

        this.mPlanteLayout.addView(image);
    }

    private void positionnerImage(PlanteImage planteImage) {
        int indexY = planteImage.getIndex() / mPlateau.getX();
        int indexX = planteImage.getIndex() - (indexY * mPlateau.getX());

        int realLargeur = this.getTailleImage(planteImage.getPlante().getDiametre());
        int realHauteur = getTailleImage(planteImage.getPlante().getHauteur());

        float multiplicateurTailleX = getMultiplicateurTaille(planteImage.getPlante().getDiametre());
        float multiplicateurTailleY = getMultiplicateurTaille(planteImage.getPlante().getHauteur());

        int realX = getXposition(multiplicateurTailleX, indexX) + 250;
        int realY = getYposition(multiplicateurTailleY, indexY) + 300;

        planteImage.getImage().setImageDrawable(mContext.getDrawable(getIdDrawable(planteImage.getPlante().getIdAffichage())));
        planteImage.getImage().getBackground().setAlpha(0);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(OutilVue.dpFromPx(realLargeur, this.mContext), OutilVue.dpFromPx(realHauteur, this.mContext));
        planteImage.getImage().setScaleType(ImageView.ScaleType.FIT_XY);

        lp.setMargins(realX, realY, 0, 0);
        planteImage.getImage().setLayoutParams(lp);

    }

    private int getIdDrawable(int idImage) {
        switch (idImage) {
            case 1:
                return R.drawable.plante_arbre_1;

            case 2:
                return R.drawable.plante_arbre_2;

            case 3:
                return R.drawable.plante_arbre_3;

            case 4:
                return R.drawable.plante_arbre_4;

            case 5:
                return R.drawable.plante_arbre_mort_1;

            case 6:
                return R.drawable.plante_arbre_mort_2;

            case 7:
                return R.drawable.plante_sapin_1;

            case 8:
                return R.drawable.plante_sapin_2;

            case 9:
                return R.drawable.plante_sapin_3;

            default:
                return 0;
        }
    }

    private int getTailleImage(int taille) {
        // 50 px = 1m
        // 100 = 2
        int value;
        if (taille < 1) {
            value = 50 * taille;
        } else {
            value = Math.round(50 * (taille - getSurpluTaille(taille)));
        }
        return value;
    }

    private float getMultiplicateurTaille(int taille) {
        float multiplicateur;
        if (taille < 1) {
            multiplicateur = taille;
        } else {
            multiplicateur = (taille - getSurpluTaille(taille));
        }
        return multiplicateur;
    }

    private float getSurpluTaille(int taille) {
        float surplu = taille - 2;
        surplu = surplu / 2;
        return surplu;
    }

    private int getYposition(float multiHauteur, int indexY) {
        //int realY = OutilVue.dpFromPx(((y * mPlateau.getTailleImage()) - (mPlateau.getTailleImage()/3)) , this.mContext);
        float realY = OutilVue.dpFromPx(((indexY * mPlateau.getTailleImage()) - (mPlateau.getTailleImage() / 3)), this.mContext);
        realY = realY - (50 * ((multiHauteur - 1) * 5 / 2));
        return Math.round(realY);
    }

    private int getXposition(float multiLargeur, int indexX) {
        float realX = OutilVue.dpFromPx((indexX * mPlateau.getTailleImage()), this.mContext);
        realX = realX - (50 * ((multiLargeur - 1)));

        return Math.round(realX);
    }

    public void majTagFiltre(GestionTags gestionTags) {
        this.mControllerFilter.setSpinnerTag(mContext, gestionTags);
    }


    public interface ControllerPlanteListener {
        public void ICliquePlante(Plante plante);
    }

    public void majPlanteAffichage(Plante plante) {
        for (PlanteImage planteImage : this.mControllerFilter.getList()) {
            if (planteImage.getPlante().getId().equals(plante.getId())) {
                this.positionnerImage(planteImage);
            }
        }
    }

}

