package creat.forestgarden.Activity.Controllers;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

import creat.forestgarden.Adapter.TagSpinnerAdapter;
import creat.forestgarden.Modele.Plantes.Tags.GestionTags;
import creat.forestgarden.Modele.Plantes.Tags.Tag;
import creat.forestgarden.Activity.Vues.TagSpinner;
import creat.forestgarden.R;

public class ControllerFilter {

    private Spinner mSpinnerTag;
    private Spinner mSpinnerTaillePlante;

    private Button mButtonvaliderFiltre;

    private TagSpinnerAdapter mTagSpinnerAdapter;
    private ControllerPlante mControllerPlante;
    private ArrayList<PlanteImage> mListPlanteImage;

    public ControllerFilter(View vue, GestionTags gestionTags, ControllerPlante controllerPlante) {
        mListPlanteImage = new ArrayList<>();
        mControllerPlante = controllerPlante;
        setSpinner(vue, gestionTags);
        setButton(vue);
    }

    private void setButton(View vue) {
        mButtonvaliderFiltre = vue.findViewById(R.id.validerFiltre);
        mButtonvaliderFiltre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majAffichage();
            }
        });
    }

    public void addPlanteImage(PlanteImage planteImage) {
        this.mListPlanteImage.add(planteImage);
        planteImage.setVisibilite(estAffichable(planteImage.getPlante().getTags()));
    }

    private void setSpinner(View vue, GestionTags gestionTags) {
        mSpinnerTag = vue.findViewById(R.id.tagfilterspinner);
        //mSpinnerTaillePlante = vue.findViewById(R.id.tagfiltertaillearbre);
        setSpinnerTag(vue.getContext(), gestionTags);

    }

    public void setSpinnerTag(Context context, GestionTags gestionTags) {
        mTagSpinnerAdapter = new TagSpinnerAdapter(context, gestionTags.getTagList(), true);
        mSpinnerTag.setAdapter(mTagSpinnerAdapter);
    }

    public void majAffichage() {
        for (PlanteImage planteimage : mListPlanteImage) {
            planteimage.setVisibilite(estAffichable(planteimage.getPlante().getTags()));
        }
    }

    public boolean estAffichable(ArrayList<Tag> tags) {

        boolean estAffiche = true;

        for (Tag tag : tags) {
            for (int i = 1; i < mTagSpinnerAdapter.getCount(); i++) {
                TagSpinner tagSpinner = (TagSpinner) mTagSpinnerAdapter.getItem(i);
                if (tag.getNom().equals(tagSpinner.getTagNom())) {
                    if (tagSpinner.isCoche()) {
                        return true; // si il est coché on a plus besoin de repasser un seul tag est suffisant pour afficher
                    } else {
                        estAffiche = false;
                        break;
                    }
                }
            }
        }

        return estAffiche;
    }

    public ArrayList<PlanteImage> getList() {
        return this.mListPlanteImage;
    }
}
