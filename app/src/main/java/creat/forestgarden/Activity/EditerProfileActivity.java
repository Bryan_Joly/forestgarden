package creat.forestgarden.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;


import creat.forestgarden.Modele.Profile;
import creat.forestgarden.Outils.OutilController;
import creat.forestgarden.R;

public class EditerProfileActivity extends AppCompatActivity implements Serializable {

    public final static String NON_COMMUNIQUE = "non communiqué";
    public final static int RESULT_LOAD_IMG = 1;

    private ImageView mImageProfile;

    private ImageButton mButtonSauvegarderProfile;
    private ImageButton mButtonQuitterProfile;

    private EditText mInputPseudo;
    private EditText mInputEmail;
    private EditText mInputRegion;

    private Profile mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editer_profile);

        mProfile = this.getProfileFromJson();
        setUI();

    }

    private void setUI() {
        setImages();
        setEditTexts();
        setButtons();
    }

    private void setButtons() {
        this.mButtonSauvegarderProfile = findViewById(R.id.saveProfile);
        this.mButtonQuitterProfile = findViewById(R.id.quitterEditProfile);

        this.mButtonSauvegarderProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButton();
            }
        });

        this.mButtonQuitterProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void saveButton() {
        this.mProfile.setPseudo(this.mInputPseudo.getText().toString());
        this.mProfile.setRegion(this.mInputRegion.getText().toString());
        this.mProfile.setEmail(this.mInputEmail.getText().toString());

        this.sauvegarderProfile();

        Toast.makeText(this.getApplicationContext(), "Donnés enregistrées", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void setEditTexts() {
        this.mInputPseudo = findViewById(R.id.inputpseudo);
        this.mInputRegion = findViewById(R.id.inputRegion);
        this.mInputEmail = findViewById(R.id.inputemail);

        this.mInputPseudo.setText(mProfile.getPseudo());

        if (mProfile.getRegion().isEmpty()) {
            this.mInputRegion.setText(NON_COMMUNIQUE);
        } else {
            this.mInputRegion.setText(mProfile.getRegion());
        }

        if (mProfile.getEmail().isEmpty()) {
            this.mInputEmail.setText(NON_COMMUNIQUE);
        } else {
            this.mInputEmail.setText(mProfile.getEmail());
        }

    }

    private void setImages() {
        this.mImageProfile = findViewById(R.id.imageProfile);
        this.mImageProfile.setImageResource(R.drawable.avatar);
    }

    public void sauvegarderProfile() {
        Gson gson = new Gson();
        String str = gson.toJson(this.mProfile);
        OutilController.sauvegarderFichier(this, str, Profile.FICHIER_UTILISATEUR);
    }

    public Profile getProfileFromJson() {
        Gson gson = new Gson();
        String str = OutilController.chargerFichier(this, Profile.FICHIER_UTILISATEUR); //deplacer dans controller
        Profile profile = gson.fromJson(str, Profile.class);
        if (profile == null) {
            this.mProfile = new Profile(Profile.CREATEUR_ANONYME);
            sauvegarderProfile();
        }
        return profile;
    }

}