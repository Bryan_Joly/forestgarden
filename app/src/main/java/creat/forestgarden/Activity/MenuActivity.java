package creat.forestgarden.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import creat.forestgarden.Modele.GestionProjets;
import creat.forestgarden.Modele.Plantes.Tags.GestionEspeces;
import creat.forestgarden.Modele.Profile;
import creat.forestgarden.Modele.Projet;
import creat.forestgarden.Modele.ProjetCaracteristique;
import creat.forestgarden.Outils.OutilController;
import creat.forestgarden.Outils.OutilVue;
import creat.forestgarden.R;

public class MenuActivity extends AppCompatActivity {

    public final static int REQUEST_CREER_PROJET = 0;
    public final static int REQUEST_PROJET_CREE = 1;
    public final static int REQUEST_STOP_CREER_PROJET = 2;


    private ImageButton mCreerButton;
    private TextView mTextNbrProjet;
    private LinearLayout mListLayout;
    private ImageButton mEditProfileButton;

    private GestionProjets mGestionProjets;
    private Profile mProfile;
    private TextView mPseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setUI();
        GestionEspeces.ajouterEspeceDEVELOP();

    }

    @Override
    protected void onStart() {
        super.onStart();
        this.mProfile = OutilController.getProfileFromFichier(this);
        mGestionProjets = OutilController.getProjetsFromJson(this);

        setButtonsValues();
        setTextsValue();
        creerListLayout();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == REQUEST_PROJET_CREE) {
            Bundle bundle = data.getExtras();
            Projet projet = (Projet) bundle.get(CreerProjetActivity.EXT_PROJET);

            this.mGestionProjets.ajouterProjet(projet);
            OutilController.sauvegarderProjets(this, this.mGestionProjets);
            Toast.makeText(this, OutilVue.TOAST_PROJET_CREER, Toast.LENGTH_SHORT).show();
        }
    }

    private void setUI() {
        setButtons();
        setTexts();
        setLayouts();
    }

    private void setLayouts() {
        this.mListLayout = findViewById(R.id.listLayout);
    }

    private void creerListLayout() {
        mListLayout.removeAllViews();

        int i = 0;
        for (final Projet projet : mGestionProjets.getListProjet()) {
            View view;
            i = i + 1;
            if (i > 1) {
                i = 0;
                view = creatItemList(projet.getProjetCarac(), R.color.blueListe);

            } else {
                view = creatItemList(projet.getProjetCarac(), R.color.white);
            }
            mListLayout.addView(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ProjetActivity.class);
                    intent.putExtra(CreerProjetActivity.EXT_PROJET, projet);
                    startActivity(intent);
                }
            });
        }
    }

    private View creatItemList(ProjetCaracteristique projetCarac, int color) {
        View itemList =
                getLayoutInflater().inflate(R.layout.list_item_projet, null);
        ((TextView) itemList.findViewById(R.id.nomCreateur)).setText(projetCarac.getNomProjet());
        ((TextView) itemList.findViewById(R.id.titreProjet)).setText(projetCarac.getNomCreateur());
        ((TextView) itemList.findViewById(R.id.region)).setText(projetCarac.getRegion());
        ((TextView) itemList.findViewById(R.id.tailleterrain)).setText(projetCarac.getTaille());
        LinearLayout ll = itemList.findViewById(R.id.layoutItemListProjet);
        ll.setBackgroundColor(getResources().getColor(color));
        ll.setAlpha(0.95f);

        return itemList;


    }

    private void setButtons() {
        this.mCreerButton = findViewById(R.id.buttonStartActivityCreerProjet);
        this.mEditProfileButton = findViewById(R.id.buttonProfile);
    }

    private void setButtonsValues() {
        setCreerButton();
        setEditProfileButton();
    }

    private void setEditProfileButton() {
        mEditProfileButton.setImageResource(R.drawable.avatar);
        mEditProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, EditerProfileActivity.class);

                startActivity(intent);
            }
        });
    }

    private void setCreerButton() {
        mCreerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, CreerProjetActivity.class);

                startActivityForResult(intent, MenuActivity.REQUEST_CREER_PROJET);

            }
        });
    }

    private void setTexts() {
        this.mTextNbrProjet = findViewById(R.id.nbrProjet);
        this.mPseudo = findViewById(R.id.pseudoInMenu);


    }

    private void setTextsValue() {
        String str = String.valueOf(this.mGestionProjets.getNbrProjet());
        this.mTextNbrProjet.setText(str);
        this.mPseudo.setText(this.mProfile.getPseudo());
    }


}