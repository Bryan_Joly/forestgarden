package creat.forestgarden.Activity.Vues;

import android.widget.CheckBox;

import creat.forestgarden.Modele.Plantes.Tags.Tag;

public class TagSpinner {
    private Tag mTag;
    private boolean mBool;
    private CheckBox mCheckBox;

    public TagSpinner(Tag tag, boolean bool) {
        this.mTag = tag;
        this.mBool = bool;
    }

    public void setBool(boolean value) {
        this.mBool = value;
        if (mCheckBox != null) {
            this.mCheckBox.setChecked(value);
        }

    }

    public String getTagNom() {
        return mTag.getNom();
    }

    public boolean isCoche() {
        return mBool;
    }

    public Tag getTag() {
        return mTag;
    }

    public void setCheckBox(CheckBox checkBox) {
        this.mCheckBox = checkBox;
        mCheckBox.setChecked(this.mBool);
    }
}
