package creat.forestgarden.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import creat.forestgarden.Modele.Profile;
import creat.forestgarden.Modele.Projet;
import creat.forestgarden.R;

public class CreerProjetActivity extends AppCompatActivity {

    public static final String EXT_PROJET = "EXTPROJET";

    private ImageButton mBoutonCreerProjet;
    private ImageButton mBoutonRetour;
    private Spinner mSpinnerCreateur;
    private EditText mTextNom;
    private EditText mTailleX;
    private EditText mTailleY;
    private EditText mTextDescription;
    private EditText mRegion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_projet);

        setUI();
    }

    private void setUI() {
        setButtonCreerProjet();
        setButtonRetour();
        setUITexts();
        setEditText();
        setSpinner();
    }

    private void setSpinner() {
        ArrayList<String> listNom = new ArrayList<String>();
        listNom.add("pseudo");
        listNom.add(Profile.CREATEUR_ANONYME);
        this.mSpinnerCreateur = findViewById(R.id.spinnerCreateur);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listNom);
        mSpinnerCreateur.setAdapter(arrayAdapter);
    }

    private void setEditText() {
        mTextNom = findViewById(R.id.inputNomProjet);
        mTailleX = findViewById(R.id.inputTailleX);
        mTailleY = findViewById(R.id.inputTailleY);
        mTextDescription = findViewById(R.id.inputDescriptionProjet);
        mRegion = findViewById(R.id.inputRegion);
    }

    private void setButtonCreerProjet() {
        mBoutonCreerProjet = findViewById(R.id.buttonStartActivityCreerProjet);

        mBoutonCreerProjet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInputValide()) {
                    Projet projet = getProjetDepuisVue();
                    commencerProchaineActivite(projet);
                } else {
                    afficherToast("La taille du terrain doit etre un chiffre", Toast.LENGTH_LONG);
                }
            }
        });
    }

    private void commencerProchaineActivite(Projet projet) {

        Intent intent = new Intent(CreerProjetActivity.this, ProjetActivity.class);
        intent.putExtra(EXT_PROJET, projet);
        setResult(MenuActivity.REQUEST_PROJET_CREE, intent);
        super.onBackPressed();
    }

    private void afficherToast(String value, int duree) {
        Toast.makeText(this, value, duree).show();
    }

    private void setButtonRetour() {
        mBoutonRetour = findViewById(R.id.ButtonLeaveActivityCreerProjet);

        mBoutonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAct();
            }
        });
    }

    private void stopAct() {
        setResult(MenuActivity.REQUEST_STOP_CREER_PROJET);
        finish();
    }

    private void setUITexts() {
        this.mTextNom = findViewById(R.id.inputNomProjet);
        this.mTextDescription = findViewById(R.id.inputDescriptionProjet);
    }

    private boolean isInputValide() {
        int x;
        int y;
        try {
            x = Integer.parseInt(String.valueOf(mTailleX.getText()));
            y = Integer.parseInt(String.valueOf(mTailleY.getText()));
            return true;
        } catch (NumberFormatException e) {

            return false;
        }

    }

    private Projet getProjetDepuisVue() {
        long valeur = mSpinnerCreateur.getSelectedItemId();
        Projet projet = new Projet(mTextNom.getText().toString(), parseString(mTailleX), parseString(mTailleY),
                mRegion.getText().toString(), valeur, mTextDescription.getText().toString());
        return projet;
    }

    private int parseString(EditText value) {
        return Integer.parseInt(value.getText().toString());
    }
}