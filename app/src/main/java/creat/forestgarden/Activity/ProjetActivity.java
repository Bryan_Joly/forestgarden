package creat.forestgarden.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import creat.forestgarden.Adapter.MapAdapter;
import creat.forestgarden.Activity.Controllers.ControllerPlante;
import creat.forestgarden.Modele.GestionPlateau.Plateau;
import creat.forestgarden.Modele.GestionProjets;
import creat.forestgarden.Modele.Plantes.Plante;
import creat.forestgarden.Modele.Plantes.Tags.GestionTags;
import creat.forestgarden.Modele.Projet;
import creat.forestgarden.Outils.OutilController;
import creat.forestgarden.Outils.OutilVue;
import creat.forestgarden.R;

public class ProjetActivity extends AppCompatActivity implements ControllerPlante.ControllerPlanteListener {

    public static final String PROJET_OUVERT = "PROJET_OUVERT";

    private GridView mGrid;

    private ImageButton mBoutonAjouterPlante;

    private Projet mProjet;
    private Plateau mPlateau;
    private LinearLayout mFiltreLayout;
    private ControllerPlante mControllerPlante;
    //Plateau plateau;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projet);

        setProjetFromIntent();

        setUI();
        View vueFiltre = setLayoutFilter();
        ImageButton button = findViewById(R.id.annulerPlante);
        mControllerPlante = new ControllerPlante(getLayoutPlante(), this, this.mProjet, vueFiltre, button, this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == PlanteActivity.REQUEST_PLANTE_CREE) {
            Bundle bundle = data.getExtras();
            Plante plante = (Plante) bundle.get(PlanteActivity.EXT_PLANTE);

            mControllerPlante.setPlacementArbre(true, plante);
            afficherToastCliquerPourPlacer();

        } else if (resultCode == PlanteActivity.REQUEST_PLANTR_CREE_PLUS_TAG) {
            Bundle bundle = data.getExtras();
            Plante plante = (Plante) bundle.get(PlanteActivity.EXT_PLANTE);
            GestionTags gestionTags = (GestionTags) bundle.get(PlanteActivity.EXT_GESTION_TAG_MODIF);
            this.mProjet.setGestionTag(gestionTags);
            this.mControllerPlante.majTagFiltre(gestionTags);

            mControllerPlante.setPlacementArbre(true, plante);
            afficherToastCliquerPourPlacer();
        } else if (resultCode == PlanteActivity.REQUEST_PLANTE_MODIF) {
            Bundle bundle = data.getExtras();
            Plante plante = (Plante) bundle.get(PlanteActivity.EXT_PLANTE);
            this.mPlateau.modifierPlante(plante);
            this.mControllerPlante.majPlanteAffichage(plante);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        sauverProjet();
    }

    private View setLayoutFilter() {
        mFiltreLayout = findViewById(R.id.layoutfiltre);

        View vue = LayoutInflater.from(this).inflate(R.layout.filtre_layout, null);
        LinearLayout layoutFiltre = vue.findViewById(R.id.layoutfilterspinner);
        LinearLayout filtreRootPanel = vue.findViewById(R.id.filtreRootPanel);
        layoutFiltre.setAlpha(0.8f);
        setButtonFiltrelayout(filtreRootPanel, layoutFiltre);
        //setContentView(R.layout.filtre_layout);
        mFiltreLayout.addView(vue);
        return vue;

    }

    private void setButtonFiltrelayout(final LinearLayout layoutRoot, final LinearLayout layoutFiltre) {
        final ImageButton boutonCacher = layoutRoot.findViewById(R.id.cacherfilterPanel);
        final ImageButton boutonMontrer = layoutRoot.findViewById(R.id.montrerfilterPanel);

        boutonCacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutFiltre.setVisibility(View.INVISIBLE);
                boutonCacher.setVisibility(View.INVISIBLE);
                boutonMontrer.setVisibility(View.VISIBLE);
            }
        });

        boutonMontrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutFiltre.setVisibility(View.VISIBLE);
                boutonMontrer.setVisibility(View.INVISIBLE);
                boutonCacher.setVisibility(View.VISIBLE);
            }
        });

        boutonMontrer.setVisibility(View.INVISIBLE);
    }


    private void afficherToastCliquerPourPlacer() {
        Toast.makeText(this, OutilVue.TOAST_PLANTE_CREE, Toast.LENGTH_LONG).show();
    }

    private void setProjetFromIntent() {
        Bundle bundle = getIntent().getExtras();
        mProjet = (Projet) bundle.get(CreerProjetActivity.EXT_PROJET);
        mPlateau = mProjet.getPlateau();
    }

    private void setUI() {
        setGrid();
        setBouton();
    }

    private RelativeLayout getLayoutPlante() {

        RelativeLayout layoutPlante = findViewById(R.id.planteLayout);
        return layoutPlante;
    }

    private void setGrid() {

        this.mGrid = findViewById(R.id.grillePlateau);

        setTailleGrid();
        creerGrid();

    }

    private void setTailleGrid() {
        ViewGroup.LayoutParams layoutPram = mGrid.getLayoutParams();
        layoutPram.width = OutilVue.dpFromPx(mPlateau.getTailleImage() * mPlateau.getX(), this);
        layoutPram.height = OutilVue.dpFromPx(mPlateau.getTailleImage() * mPlateau.getY(), this);
        this.mGrid.setLayoutParams(layoutPram);
        mGrid.requestLayout();

    }


    private void creerGrid() {

        this.mGrid.setNumColumns(mPlateau.getX());
        MapAdapter adapter = new MapAdapter(mPlateau, getApplicationContext(), this);
        this.mGrid.setAdapter(adapter);

    }

    public void setBouton() {
        this.mBoutonAjouterPlante = findViewById(R.id.ajouterPlante);
        this.mBoutonAjouterPlante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActPlanteAjout();
            }
        });
    }

    public void startActPlanteModification(Plante plante) {
        Intent intent = new Intent(ProjetActivity.this, PlanteActivity.class);
        intent.putExtra(PlanteActivity.EXT_GESTION_TAG, mProjet.getGestionTag());
        intent.putExtra(PlanteActivity.EXT_PLANTE_MODIF, plante);
        intent.setAction(PlanteActivity.ACTION_MODIF_PLANTE);
        startActivityForResult(intent, PlanteActivity.REQUEST_PLANTE);
    }

    private void startActPlanteAjout() {
        Intent intent = new Intent(ProjetActivity.this, PlanteActivity.class);
        intent.putExtra(PlanteActivity.EXT_GESTION_TAG, mProjet.getGestionTag());
        intent.setAction(PlanteActivity.ACTION_AJOUTER_PLANTE);
        startActivityForResult(intent, PlanteActivity.REQUEST_PLANTE);
    }

    private void getCellGrid(int a) {
        MapAdapter adapter = (MapAdapter) mGrid.getAdapter();
        if (a < adapter.getCount()) {
            adapter.getItem(a);
        } else {
            Toast.makeText(this.getBaseContext(), "erreurTableau adapter", Toast.LENGTH_LONG).show();
        }

    }

    public void cliqueOnCell(View view) {
        mControllerPlante.cliqueCellule(view);
    }

    public void sauverProjet() {
        GestionProjets gestionProjets = OutilController.getProjetsFromJson(this);
        gestionProjets.mettreAJourProjet(this.mProjet);
        OutilController.sauvegarderProjets(this, gestionProjets);

    }


    @Override
    public void ICliquePlante(Plante plante) {
        startActPlanteModification(plante);

    }
}