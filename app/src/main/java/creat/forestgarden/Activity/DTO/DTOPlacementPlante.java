package creat.forestgarden.Activity.DTO;

import creat.forestgarden.Modele.Plantes.Plante;

//DTO data transfert object
public class DTOPlacementPlante {
    private boolean mPlacerPlante;
    private Plante mPlante;

    public DTOPlacementPlante() {

        this.mPlacerPlante = false;
    }

    public boolean isPlacerArbre() {
        return this.mPlacerPlante;
    }

    public void setPlacerArbre(boolean bool) {
        this.mPlacerPlante = bool;
    }

    public void setPlante(Plante plante) {
        this.mPlante = plante;
    }

    public Plante getPlante() {
        return this.mPlante;
    }
}
